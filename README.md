Test runner and whitelist generator
===================================

This script is used to run CI tools like phpmd or phan. It generates a whitelist of accepted errors and allows your 
code to move forward while keeping your legacy code under control.

The first step is to define a baseline for your project. This baseline typically will be the last released
version of your software. Then you generate a whitelist of your current errors and the runner will stop you
from adding new errors to your legacy code.

At the moment you have 2 tools at your disposal, phpmd and phan. Start by enabling them in the config and 
adding them to your project if they are not there already.

Requirements
------------

* PHP
* composer

Features
--------

* Flexible configuration.
* Fast execution and diff generation.

Installation
============

    composer require jfsdev/runner
    
Copy vendor/jfsdev/runner/config.yml.dist to your root and name it config.yml.

Useful Tools
============

Running Tests:
--------
To define a baseline:

    php vendor/bin/runner.php tests:run --baseline [baseline]
 
 or 
 
    php vendor/bin/runner.php tests:run
    
to run tests against the current baseline.

Individual tools can be specified with the --tool flag.
 
    php vendor/bin/runner.php tests:run --tool phan

Changelog
=========

To keep track, please refer to [CHANGELOG.md](https://github.com/jfsdev/runner/blob/master/CHANGELOG.md).

Contributing
============

Also please refer to [CONTRIBUTION.md](https://github.com/jfsdev/runner/blob/master/CHANGELOG.md/blob/master/CONTRIBUTION.md).

License
=======

Please refer to [LICENSE](https://github.com/jfsdev/runner/blob/master/CHANGELOG.md/blob/master/LICENSE).
