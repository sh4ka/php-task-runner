#!/usr/bin/env php
<?php
$configFile = dirname(__DIR__ , 2) . DIRECTORY_SEPARATOR . 'config.yml.dist';
if (file_exists(dirname(__DIR__, 5) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')) {
    define('ROOT_DIR', dirname(__DIR__ , 5));
    if (file_exists(dirname(__DIR__ , 5) . DIRECTORY_SEPARATOR . 'config.yml')) {
        $configFile = ROOT_DIR . DIRECTORY_SEPARATOR . 'config.yml';
    }
} elseif (file_exists(dirname(__DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR .  'autoload.php' , 2))) {
    define('ROOT_DIR', dirname(__DIR__ , 2));
    if (file_exists(dirname(__DIR__ , 2) . DIRECTORY_SEPARATOR . 'config.yml')) {
        $configFile = file_exists(ROOT_DIR . DIRECTORY_SEPARATOR . 'config.yml');
    }
} else {
    die();
}

define('VENDOR_DIR', ROOT_DIR . DIRECTORY_SEPARATOR . 'vendor');
define('BIN_DIR', VENDOR_DIR . DIRECTORY_SEPARATOR . 'bin');
define('OUTPUT_DIR', ROOT_DIR . DIRECTORY_SEPARATOR . 'output');

require_once VENDOR_DIR . DIRECTORY_SEPARATOR . 'autoload.php';

use Symfony\Component\Console\Application;
use TestRunner\Command\TestRunnerCommand;
use TestRunner\Service\Configuration;

$config = new Configuration();
$config->loadConfigurationFile($configFile);

$application = new Application();

$application->add(new TestRunnerCommand($config->get('test-runner')));

$application->run();
