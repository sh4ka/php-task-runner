<?php

namespace TestRunner\Runners;

abstract class AbstractRunner
{
    abstract public function run();
    abstract public function getName();
    abstract public function isError(string $newValue, array $previousValues);
}