<?php

namespace TestRunner\Runners;

class PhanRunner extends AbstractRunner
{
    const NAME = 'phan';
    private $configuration;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    public function run()
    {
        $command = VENDOR_DIR . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . self::NAME;
        $desiredOutput = ' -m text ';
        $options = '';

        $execLine = $command . $desiredOutput . $options;

        exec($execLine, $output);

        return $output;
    }

    public function getName()
    {
        return self::NAME;
    }

    public function isError(string $newValue, array $oldValues)
    {
        $found = false;
        $replaceLineNumber = '/([:]\d+)/';
        $newTrimmedLine = preg_replace($replaceLineNumber, '', $newValue);
        //$newTrimmedLine = str_replace(ROOT_DIR, '', $newTrimmedLine);
        $newTrimmedLine = str_replace('\\', '/', $newTrimmedLine);
        foreach ($oldValues as $oldLine) {
            $oldTrimmedLine = preg_replace($replaceLineNumber, '', $oldLine);
            if ($oldTrimmedLine === $newTrimmedLine) {
                $found = true;
                break;
            }
        }

        return !$found;
    }

}