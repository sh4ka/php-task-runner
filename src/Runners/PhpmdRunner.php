<?php

namespace TestRunner\Runners;

class PhpmdRunner extends AbstractRunner
{
    const NAME = 'phpmd';
    private $configuration;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    public function run()
    {
        $command = BIN_DIR . DIRECTORY_SEPARATOR . 'phpmd';
        $sourceLocation = ROOT_DIR . DIRECTORY_SEPARATOR . $this->configuration['source'] ?? 'src';
        $desiredOutput = 'text';
        $options = 'cleancode,codesize,naming,unusedcode';

        $execLine = $command . ' ' . $sourceLocation . ' ' . $desiredOutput . ' ' . $options;

        exec($execLine, $output);

        return $output;
    }

    public function getName()
    {
        return self::NAME;
    }

    public function isError(string $newValue, array $oldValues)
    {
        $found = false;
        $replaceLineNumber = '/([:]\d+)/';
        $newTrimmedLine = preg_replace($replaceLineNumber, '', $newValue);
        $newTrimmedLine = str_replace(ROOT_DIR, '', $newTrimmedLine);
        $newTrimmedLine = str_replace('\\', '/', $newTrimmedLine);
        foreach ($oldValues as $oldLine) {
            $oldTrimmedLine = preg_replace($replaceLineNumber, '', $oldLine);
            if ($oldTrimmedLine === $newTrimmedLine) {
                $found = true;
                break;
            }
        }

        return !$found;
    }

}