<?php

namespace TestRunner\Command;

use DirectoryIterator;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TestRunner\AbstractRunner;
use TestRunner\Runner;
use TestRunner\Service\Configuration;
use TestRunner\Service\FileDiff;
use TestRunner\Service\FileWritingService;

class TestRunnerCommand extends Command
{
    protected static $defaultName = 'tests:run';
    protected $outputLocation = DIRECTORY_SEPARATOR . 'output';
    protected $configuration;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addOption(
                'baseline',
                null,
                InputOption::VALUE_OPTIONAL,
                'If this is the first run, enter the current tag name',
                ''
            )
            ->addOption(
                'tool',
                null,
                InputOption::VALUE_OPTIONAL,
                'If this is set, try to parse the tool and use a basic run',
                ''
            )
            ->setDescription('Runs all configured commands.')
            ->setHelp('This command runs all the configured tests');
    }

    /**
     * @todo refactor this thing
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Running CI/CD Suite');

        $baseline = $input->getOption('baseline');
        $tools = $this->initTools($input->getOption('tool'));

        $fileWriter = new FileWritingService($baseline);
        $fileWriter->initDirectories();
        if (empty($fileWriter->getBaseline())) {
            $io->error("Run with flag --baseline <tag> first");
            return false;
        }

        $output = [];
        /** @var AbstractRunner $tool */
        foreach ($tools as $tool) {
            $io->section('Running ' . $tool->getName());
            $output[$tool->getName()] = $tool->run();
            $io->success($tool->getName() . ' completed');

            if (!empty($baseline)) {
                $fileWriter->write($output[$tool->getName()], $tool->getName());
            }
        }

        $exitValue = 0;
        if (empty($baseline)) {
            $fileComparator = new FileDiff($fileWriter->getBaseline());
            /** @var AbstractRunner $tool */
            foreach ($tools as $tool) {
                $diff = $fileComparator->getDiffResult($output[$tool->getName()], $tool->getName());
                if (count($diff) > 0) {
                    // files differ, lets do an inspect
                    $previous = file(realpath($fileWriter->getBaseline() . DIRECTORY_SEPARATOR . $tool->getName()),
                        FILE_IGNORE_NEW_LINES);
                    /** @var \Diff\DiffOp\DiffOpChange $item */
                    foreach ($diff as $item) {
                        if (get_class($item) === 'Diff\DiffOp\DiffOpChange' && $tool->isError($item->getNewValue(), $previous)) {
                            // tell if it is really a difference by comparing with the other errors
                            $exitValue = 1;
                            $io->text($item->getNewValue());
                        }
                    }
                    $exitValue === 1 ? $io->error($tool->getName() . ' differs') : $io->success($tool->getName() . ' passed');
                }
            }

            $isDryRun = $this->configuration['dry-run'] ?? false;
            if (empty($baseline)) {
                ($isDryRun || $exitValue === 0) ?
                    $io->success('Execution is equal to baseline') :
                    $io->error('Execution differs to baseline');
            }
        }

        exit($exitValue);
    }

    protected function initTools($baseTool = '')
    {
        $configTools = array_map(function ($tool, $value){
            if ($value === true) {
                return $tool;
            }

            return false;
        }, array_keys($this->configuration['tools']), array_values($this->configuration['tools']));

        if (!empty($baseTool)) {
            $configTools = [$baseTool];
        }

        $instances = [];
        foreach ($configTools as $tool) {
            if ($tool !== false) {
                $className = 'TestRunner\\Runners\\' .  ucfirst(strtolower($tool)) . 'Runner';
                $instances[] = new $className($this->configuration);
            }
        }

        return $instances;
    }
}
