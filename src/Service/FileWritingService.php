<?php

namespace TestRunner\Service;

use DirectoryIterator;
use InvalidArgumentException;

class FileWritingService
{
    private $baseline;

    public function __construct(string $directory = '')
    {
        $this->baseline = $directory;
    }

    public function initDirectories()
    {
        if (!empty($this->baseline)) {
            // baseline run, create everything
            if (!is_dir(OUTPUT_DIR)) {
                $this->makeOutputDir();
            }
            $this->emptyOutputDir();
            $this->makeBaselineDir();
        }
    }

    public function emptyOutputDir()
    {
        $dir = $this->getBaseline();
        if (file_exists(dirname($dir))) {
            $targetDir = new DirectoryIterator(dirname($dir));
            // baseline run, delete old runs
            foreach ($targetDir as $fileinfo) {
                if (
                    !$fileinfo->isDot() &&
                    substr($fileinfo->getFilename(), 0, 1) !== '.'
                ) {
                    // todo: catch here in case there is something unexpected
                    $this->deleteDir($fileinfo->getRealPath());
                }
            }
        }
    }

    public function makeOutputDir()
    {
        if (file_exists(OUTPUT_DIR) === false) {
            mkdir(OUTPUT_DIR, 0777);
        }
    }

    public function makeBaselineDir()
    {
        $baselineDir = $this->getBaseline();
        if (file_exists($baselineDir) === false) {
            mkdir($baselineDir, 0777, true);
        }
    }

    public function write(array $content, string $type): bool
    {
        $handle = fopen($this->getBaseline() . DIRECTORY_SEPARATOR . $type, 'w+');
        if (is_array($content)) {
            foreach ($content as $lineToWrite) {
                $lineToWrite = str_replace(ROOT_DIR, '', $lineToWrite);
                $lineToWrite = str_replace('\\', '/', $lineToWrite); //todo: be mindful of what we replace
                fwrite($handle, $lineToWrite);
                fwrite($handle, PHP_EOL);
            }
        }
        fclose($handle);

        return true;
    }

    protected function deleteDir($dirPath)
    {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != DIRECTORY_SEPARATOR) {
            $dirPath .= DIRECTORY_SEPARATOR;
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    public function getBaseline()
    {
        if (empty($this->baseline) && file_exists(OUTPUT_DIR)) {
            $dir = new DirectoryIterator(OUTPUT_DIR);
            foreach ($dir as $fileinfo) {
                if (!$fileinfo->isDot() &&
                    is_dir($fileinfo->getRealPath()) &&
                    strstr($fileinfo->getRealPath(), 'baseline')
                ) {
                    return $fileinfo->getRealPath();
                }
            }
        }

        return OUTPUT_DIR . DIRECTORY_SEPARATOR . 'baseline_' . $this->baseline;
    }
}
