<?php

namespace TestRunner\Service;

use Diff\Differ\MapDiffer;
use TestRunner\Library\Diff;

class FileDiff
{
    protected $previous;

    public function __construct(string $previous = null)
    {
        $this->previous = $previous;
    }

    public function getDiffResult(array $outputData, string $type = 'phpmd'): array
    {
        $diff = new MapDiffer();
        $computedDiff = [];
        $file = realpath($this->previous . '/' . $type);
        if (!empty($file) && file_exists($file)) {
            $previous = file($file, FILE_IGNORE_NEW_LINES);
            $computedDiff = $diff->doDiff($previous, $outputData);
        }

        return $computedDiff;
    }
}
