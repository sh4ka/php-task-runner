<?php

namespace TestRunner\Service;

use Symfony\Component\Yaml\Yaml;

class Configuration
{
    private $configuration;
    private $configurationFile;

    public function __construct($file = null)
    {
        if ($file) {
            $this->configurationFile = $file;
        }
    }

    public function loadConfigurationFile($file)
    {
        $this->configuration = Yaml::parseFile($file);
    }

    public function get($value)
    {
        return isset($this->configuration[$value]) ? $this->configuration[$value] : null;
    }
}